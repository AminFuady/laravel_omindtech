<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Guru;
use Response;

class GuruController extends Controller
{
    public function index()
    {
        $guru = Guru::all();

        $response = [
            'status' => 'success',
            'data' => $guru
        ];

        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
    	$guru = new Guru;

    	$guru->nama = $request->nama;
    	$guru->alamat = $request->alamat;
    	$guru->jenis_kelamin = $request->jenis_kelamin;
    	$guru->agama = $request->agama;
    	$guru->mapel = $request->mapel;

    	$success = $guru->save();

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $guru
        ];

        return response()->json($response, 200);

    }

    public function update(Request $request, $id)
    {
        $guru = Guru::findOrFail($id)->update($request->all());

        $response = [
            'status'    =>  'success',
            'message'   =>  'Record updated successfully.',
            'data'      =>  Guru::find($id)
        ];
        return response()->json($response, 200);
    }

    public function destroy($id)
    {
        $guru = Guru::findOrFail($id)->delete($id);

        $response = [
            'status'  => 'success',
            'message' => 'Record delete successfully.'
        ];
        return response()->json($response, 200);
    }
}
