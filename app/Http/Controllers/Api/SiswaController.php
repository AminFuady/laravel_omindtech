<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Siswa;
use Response;


class SiswaController extends Controller
{

    public function index()
    {
        $siswa = Siswa::all();

        $response = [
            'status' => 'success',
            'data' => $siswa
        ];

        return response()->json($response, 200);
    }

    public function store(Request $request)
    {
    	$siswa = new Siswa;

    	$siswa->nama = $request->nama;
    	$siswa->alamat = $request->alamat;
    	$siswa->jenis_kelamin = $request->jenis_kelamin;
    	$siswa->agama = $request->agama;

    	$siswa->save();

        $response = [
            'status' => 'success',
            'message' => 'Record created successfully.',
            'data' => $siswa
        ];

        return response()->json($response, 200);

    }

    public function update(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id)->update($request->all());

        $response = [
            'status'    =>  'success',
            'message'   =>  'Record updated successfully.',
            'data'      =>  Siswa::find($id)
        ];
        return response()->json($response, 200);
    }

    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id)->delete($id);

        $response = [
            'status'  => 'success',
            'message' => 'Record delete successfully.'
        ];
        return response()->json($response, 200);
    }
}
